import { FuncObj } from './types';




const getTabSF = (parentNum: number, num: number, belongSystem: string, funcNum: string): FuncObj => {
  let name: string = '';
  if (parentNum == 0 && num == 1) {
    name = '客户管理';
  } else if (parentNum == 0 && num == 2) {
    name = '项目管理';
  } else if (parentNum == 0 && num == 3) {
    name = '药品目录';
  } else if (parentNum == 0 && num == 4) {
    name = '订单管理';
  } else if (parentNum == 0 && num == 5) {
    name = '所有商机';
  } else if (parentNum == 0 && num == 6) {
    name = '公告管理';
  } else if (parentNum == 0 && num == 7) {
    name = '消息记录';
  }

  if (parentNum == 1 && num == 1) {
    name = '反馈管理';
  }

  if (parentNum == 2 && num == 1) {
    name = '系统管理';
  } else if (parentNum == 2 && num == 2) {
    name = '配置管理';
  } else if (parentNum == 2 && num == 3) {
    name = '菜单管理';
  } else if (parentNum == 2 && num == 4) {
    name = '系统日志';
  }
  if (name == '' || !name) {
    let childItems!: FuncObj;
    return childItems;
  }
  const tabItem: FuncObj = {
    id: num,
    funcNum: 'func20' + num,
    funcName: name,
    aliasName: name,
    requestUrl: '',
    belongSystem: belongSystem,
    level: 2,
    funcType: 'SYSTEM',
    routePath: '',
    sort: num,
    parentFuncNum: funcNum,
    displayTerminal: 'WEB',
    children: [],
    getShowText: () => { return name; }
  }
  return tabItem;
}

const getTabSFS = (sP: number, parentNum: number, num: number, belongSystem: string, funcNum: string): FuncObj => {
  let name: string = '';
  let routePath: string = '';
  if (sP == 0) {
    if (parentNum == 1 && num == 1) {
      name = '资质审批';
      routePath = '/operate/customer/qualityApprove';
    } else if (parentNum == 1 && num == 2) {
      name = '客户管理';
      routePath = '/operate/custome/customerManage';
    }
    if (parentNum == 2 && num == 1) {
      name = '项目管理';
      routePath = '/operate/project/projectManage';
    }
    if (parentNum == 3 && num == 1) {
      name = '药品列表';
      routePath = '/operate/drug/drugList';
    } else if (parentNum == 3 && num == 2) {
      name = '添加药品';
      routePath = '/operate/drug/addDrug';
    } else if (parentNum == 3 && num == 3) {
      name = '药品分类';
      routePath = '/operate/drug/drugVariety';
    } else if (parentNum == 3 && num == 4) {
      name = '药品类型';
      routePath = '/operate/drug/drugCategory';
    } else if (parentNum == 3 && num == 5) {
      name = '品牌管理';
      routePath = '/operate/drug/drugBrand';
    }
    if (parentNum == 4 && num == 1) {
      name = '订单列表';
      routePath = '/operate/order/orderList';
    } else if (parentNum == 4 && num == 2) {
      name = '订单设置';
      routePath = '/operate/order/orderSetting';
    } else if (parentNum == 4 && num == 3) {
      name = '退货申请处理';
      routePath = '/operate/order/returnGoodsManage';
    } else if (parentNum == 4 && num == 4) {
      name = '退货原因设置';
      routePath = '/operate/order/returnGoodsReason';
    }

    if (parentNum == 5 && num == 1) {
      name = '所有商机';
      routePath = '/operate/business/allBusiness';
    } else if (parentNum == 5 && num == 2) {
      name = '我的商机';
      routePath = '/operate/business/myBusiness';
    }

    if (parentNum == 6 && num == 1) {
      name = '平台公告';
      routePath = '/operate/notice/platformNotice';
    } else if (parentNum == 6 && num == 2) {
      name = '客户公告';
      routePath = '/operate/notice/customerNotice';
    }

    if (parentNum == 7 && num == 1) {
      name = '邮件发送记录';
      routePath = '/operate/message/emailMessageRecord';
    } else if (parentNum == 7 && num == 2) {
      name = '短信发送记录';
      routePath = '/operate/message/shortMessageRecord';
    }
  }

  if (sP == 1) {
    if (parentNum == 1 && num == 1) {
      name = '服务需求';
      routePath = '/operate/customer/serviceDemand';
    } else if (parentNum == 1 && num == 2) {
      name = '客户表扬';
      routePath = '/operate/customer/customerPraise';
    } else if (parentNum == 1 && num == 3) {
      name = '客户建议';
      routePath = '/operate/customer/customerSuggesst';
    } else if (parentNum == 1 && num == 4) {
      name = '意见反馈';
      routePath = '/operate/customer/feedBack';
    } else if (parentNum == 1 && num == 5) {
      name = '故障排查';
      routePath = '/operate/customer/troubleShoot';
    }
  }

  if (sP == 2) {
    if (parentNum == 1 && num == 1) {
      name = '用户列表';
      routePath = '/system/system/userList';
    } else if (parentNum == 1 && num == 2) {
      name = '角色列表';
      routePath = '/system/system/roleList';
    } else if (parentNum == 1 && num == 3) {
      name = '组织管理';
      routePath = '/system/system/organizationManage';
    }

    if (parentNum == 2 && num == 1) {
      name = '接入系统';
      routePath = '/system/config/accessSystem';
    } else if (parentNum == 2 && num == 2) {
      name = '系统配置';
      routePath = '/system/config/systemConfig';
    }

    if (parentNum == 3 && num == 1) {
      name = '菜单接入管理';
      routePath = '/system/func/funcAccess';
    } else if (parentNum == 3 && num == 2) {
      name = '菜单管理';
      routePath = '/system/func/funcMenu';
    }

    if (parentNum == 4 && num == 1) {
      name = '登录日志';
      routePath = '/system/log/loginLog';
    } else if (parentNum == 4 && num == 2) {
      name = '关键日志';
      routePath = '/system/log/cruxLog';
    }
  }
  let childItems!: FuncObj;
  if (name == '' || !name) {
    return childItems;
  }
  const childItem: FuncObj = {
    id: num,
    funcNum: 'func30' + num,
    funcName: name,
    aliasName: name,
    requestUrl: 'http://81.68.221.218/test/index.html#/',
    belongSystem: belongSystem,
    level: 3,
    funcType: 'MENU',
    routePath: routePath,
    sort: num,
    parentFuncNum: funcNum,
    displayTerminal: 'WEB',
    children: [],
    getShowText: () => { return name; }
  }
  return childItem;
}

function getRouterPath(typeNum: number): string {
  if (typeNum == 1) {
    return '/'
  } else if (typeNum == 2) {
    return '/testTwo';
  } else if (typeNum == 3) {
    return '/testThree';
  } else if (typeNum == 4) {
    return '/testFour';
  } else if (typeNum == 5) {
    return '/testFive';
  } else if (typeNum == 6) {
    return '/testSix';
  } else if (typeNum == 7) {
    return '/testSeven';
  } else if (typeNum == 8) {
    return '/testEight';
  }
  return '/testNine';
}

export const FUNCLIST: Array<FuncObj> = [
  {
    id: 1,
    funcNum: 'func01',
    funcName: '运营中心',
    aliasName: '运营中心',
    requestUrl: '',
    belongSystem: '运营中心',
    level: 1,
    funcType: 'MODULE',
    routePath: 'TEST',
    sort: 1,
    parentFuncNum: '-1',
    displayTerminal: 'WEB',
    children: [],
    getShowText: () => { return '运营中心'; }
  },
  {
    id: 2,
    funcNum: 'func02',
    funcName: '客服中心',
    aliasName: '客服中心',
    requestUrl: '',
    belongSystem: 'kefucenter',
    level: 1,
    funcType: 'MODULE',
    routePath: 'TEST',
    sort: 2,
    parentFuncNum: '-1',
    displayTerminal: 'WEB',
    children: [],
    getShowText: () => { return '客服中心'; }
  },
  {
    id: 3,
    funcNum: 'func03',
    funcName: '系统中心',
    aliasName: '系统中心',
    requestUrl: '',
    belongSystem: 'systemcenter',
    level: 1,
    funcType: 'MODULE',
    routePath: 'TEST',
    sort: 1,
    parentFuncNum: '-1',
    displayTerminal: 'WEB',
    children: [],
    getShowText: () => { return '系统中心'; }
  }

];


export const generateTabs = (): Array<FuncObj> => {
  const result: Array<FuncObj> = [];
  for (let n = 0; n < FUNCLIST.length; n++) {
    const module = FUNCLIST[n];
    for (let i = 1; i <= 8; i++) {
      const tabItem: FuncObj = getTabSF(n, i, module.belongSystem, module.funcNum);
      if (tabItem) {
        for (let j = 1; j <= 7; j++) {
          const childItem: FuncObj = getTabSFS(n, i, j, module.belongSystem, tabItem.funcNum);
          if (childItem) {
            tabItem.children.push(childItem);
          }
        }
        module.children.push(tabItem);
      }
    }
    result.push(module);
  }
  return result;
}

//http://81.68.221.218/test/index.html#/
//http://localhost:8081/#/