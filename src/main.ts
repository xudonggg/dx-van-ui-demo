import { createApp } from 'vue'
import App from './App.vue'
import router from "./router";
import DxVanUi from 'dx-van-ui';
import 'dx-van-ui/lib/index.full.css';
const app = createApp(App)

app
.use(router)
.use(DxVanUi)
app.mount('#app')
