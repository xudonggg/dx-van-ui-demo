import { createRouter, createWebHistory, createWebHashHistory, RouteRecordRaw, Router } from "vue-router";
import DxVanHeader from '../views/DxVanHeader.vue';
import DxBaseViewTest from '../views/DxBaseViewTest.vue';
import DxVanButtonTest from '../views/DxVanButtonTest.vue';
import DxVanCellTest from '../views/DxVanCellTest.vue';
import DxVanFieldTest from '../views/DxVanFieldTest.vue';
import DxVanNavbarTest from '../views/DxVanNavbarTest.vue';
import DxVanCellSwipTest from '../views/DxVanCellSwipTest.vue';
import DxVanSpinnerTest from '../views/DxVanSpinnerTest.vue';
import DxVanTabbarTest from '../views/DxVanTabbarTest.vue';
import DxVanTabContainerTest from '../views/DxVanTabContainerTest.vue';
import DxVanSearchTest from '../views/DxVanSearchTest.vue';
import DxVanSwitchTest from '../views/DxVanSwitchTest.vue';
import DxVanRadioTest from '../views/DxVanRadioTest.vue';
import DxVanCheckTest from '../views/DxVanCheckTest.vue';
import DxVanBadgeTest from '../views/DxVanBadgeTest.vue';
import DxVanPopupTest from '../views/DxVanPopupTest.vue';
import DxVanSwipTest from '../views/DxVanSwipTest.vue';
import DxVanActionSheetTest from '../views/DxVanActionSheetTest.vue';
import DxVanMessageBoxTest from '../views/DxVanMessageBoxTest.vue';
import DxVanProgressTest from '../views/DxVanProgressTest.vue';
import DxVanPaletteButtonTest from '../views/DxVanPaletteButtonTest.vue';
import DxVanToastTest from '../views/DxVanToastTest.vue';
import DxVanRangeTest from '../views/DxVanRangeTest.vue';
import DxVanIndicatorTest from '../views/DxVanIndicatorTest.vue';
import DxVanLazyLoadTest from '../views/DxVanLazyLoadTest.vue';
import DxVanPickerTest from '../views/DxVanPickerTest.vue';
import DxVanDateTimePickerTest from '../views/DxVanDateTimePickerTest.vue';

const routes: Array<RouteRecordRaw> = [
  
  { path: '/DxVanHeader', component: DxVanHeader },
  { path: '/DxBaseViewTest', component: DxBaseViewTest},
  { path: '/DxVanButtonTest', component: DxVanButtonTest},
  { path: '/DxVanCellTest', component: DxVanCellTest},
  { path: '/DxVanFieldTest', component: DxVanFieldTest},
  { path: '/DxVanNavbarTest', component: DxVanNavbarTest},
  { path: '/DxVanCellSwipTest', component: DxVanCellSwipTest},
  { path: '/DxVanSpinnerTest', component: DxVanSpinnerTest},
  { path: '/DxVanTabbarTest', component: DxVanTabbarTest},
  { path: '/DxVanTabContainerTest', component: DxVanTabContainerTest},
  { path: '/DxVanSearchTest', component: DxVanSearchTest},
  { path: '/DxVanSwitchTest', component: DxVanSwitchTest},
  { path: '/DxVanRadioTest', component: DxVanRadioTest},
  { path: '/DxVanCheckTest', component: DxVanCheckTest},
  { path: '/DxVanBadgeTest', component: DxVanBadgeTest},
  { path: '/DxVanPopupTest', component: DxVanPopupTest},
  { path: '/DxVanSwipTest', component: DxVanSwipTest},
  { path: '/DxVanActionSheetTest', component: DxVanActionSheetTest},
  { path: '/DxVanMessageBoxTest', component: DxVanMessageBoxTest},
  { path: '/DxVanProgressTest', component: DxVanProgressTest},
  { path: '/DxVanPaletteButtonTest', component: DxVanPaletteButtonTest},
  { path: '/DxVanToastTest', component: DxVanToastTest},
  { path: '/DxVanRangeTest', component: DxVanRangeTest},
  { path: '/DxVanIndicatorTest', component: DxVanIndicatorTest},
  { path: '/DxVanLazyLoadTest', component: DxVanLazyLoadTest},
  { path: '/DxVanPickerTest', component: DxVanPickerTest},
  { path: '/DxVanDateTimePickerTest', component: DxVanDateTimePickerTest},
];

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
});



export default router;
